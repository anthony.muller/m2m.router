var proxy = require('express-http-proxy')

var express = require('express')
var app = express()

app.use('/modlog/*', proxy('www.example.com'))

app.use('/*', proxy('www.example.com'))


app.listen(1972, () => console.log("Listening on port 1972"))

