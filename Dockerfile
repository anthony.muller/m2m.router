FROM node:8.11.2-alpine

WORKDIR /app

COPY package.json ./

RUN npm install

EXPOSE 1972

COPY index.js ./

ENTRYPOINT [ "npm", "start" ]
